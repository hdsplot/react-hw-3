const initialStateValues = {
   popupName: null,
   goods: [],
   favorites: JSON.parse(localStorage.getItem('favorites')) || [],
   cart: JSON.parse(localStorage.getItem('cart')) || [],
   currentItem: {
      name: null,
      article: null,
      color: null,
      price: null,
      image: null
   }
}

export default initialStateValues;
