import React from 'react';
import './Cart.scss'
import CartItem from "./CartItem/CartItem";
import PropTypes from 'prop-types';

const Cart = (props) => {
  const { cart, removeAction } = props;
  const cartItems = () => {
    const duplicate = cart.map(item => {
      item.quantity = cart.filter(el => el.article === item.article).length
      return item;
    });
    const codes = [...new Set(duplicate.map(item => item.article))]

    return codes.map(code => duplicate.find(item => item.article === code));
  }

  const items = cartItems();

  const renderedItems = items.map(item => {
    let { name, price, image, article, color, quantity } = item;
    return <CartItem name={name}
      price={price}
      image={image}
      article={article}
      color={color}
      quantity={quantity}
      removeAction={removeAction}
      key={article} />
  });

  return (
    <div className="cart">
      {renderedItems}
    </div>
  );
}

export default Cart;

Cart.propTypes = {
  cart: PropTypes.array,
  removeAction: PropTypes.func,
}