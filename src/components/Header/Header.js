import React from 'react';
import './Header.scss';
import PropTypes from 'prop-types';

const Header = (props) => {

  const { logo, nav } = props;
  return (
    <header className="header">
      <div className="header__logo">
        <a href="/">
          <img src={logo} alt="logotype" />
        </a>
      </div>
      {nav}
    </header>
  );
}

export default Header;

Header.propTypes = {
  logo: PropTypes.string,
  nav: PropTypes.object
}

Header.defaultProps = {
  nav: null
}