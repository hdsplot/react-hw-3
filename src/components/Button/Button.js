import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

const Button = (props) => {
  const { text, element, handler, modifier } = props;
  return (
    <button className={`btn btn--${modifier}`} onClick={handler}>{text || element}</button>
  );
}

export default Button;

Button.propTypes = {
  text: PropTypes.string,
  element: PropTypes.object,
  handler: PropTypes.func,
  modifier: PropTypes.string
}

Button.defaultProps = {
  modifier: 'std',
  element: null
}