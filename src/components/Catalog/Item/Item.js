import React from 'react';
import './Item.scss'
import Button from "../../Button/Button";
import PropTypes from 'prop-types';
import FavoriteStar from './FavoriteStar';

const Item = (props) => {

  const { name, price, image, article, color, addToFav, showModal, favorites } = props;
  const isFavorite = favorites.includes(props.article);

  return (
    <div className="product-item">
      <div className="product-item__image"><img src={image} alt={`product code: ${article}`} /></div>
      <h4 className="product-item__title">{name}</h4>
      <div className="product-item__description">
        <span className="product-item__code">{`article: ${article}`}</span>
        <span className="product-item__color">{`color: ${color}`}</span>

        <span className="product-item__price">{"price: "}
          <span className="product-item__price-sum">{price.toLocaleString(
            'USD',
            { style: 'currency', currency: 'USD', currencyDisplay: 'symbol' }
          )}
          </span>
        </span>
      </div>
      <FavoriteStar indicated={isFavorite} item={article} clickHandler={addToFav} />
      <Button modifier="cart" text="Add to cart" handler={() => showModal({ ...props })} />
    </div>
  );
}

export default Item;

Item.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  image: PropTypes.string,
  article: PropTypes.number,
  color: PropTypes.string,
  favorites: PropTypes.array,
  addToFav: PropTypes.func,
  showModal: PropTypes.func,
}