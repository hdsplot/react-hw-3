import React from "react";
import  StarIcon  from "../../Icons/StarIcon";
import PropTypes from "prop-types";

const Favorites = (props) => {
  const { favLength } = props;
  return (
    <>
      <StarIcon />
      <span>{favLength}</span>
    </>
  );
};

export default Favorites;

Favorites.propTypes = {
  favLength: PropTypes.number,
};
