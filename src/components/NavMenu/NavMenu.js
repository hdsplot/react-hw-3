import React from 'react';
import { Link } from "react-router-dom";
import './NavMenu.scss'
import Cart from './components/Cart';
import Favorites from './components/Favorites';

const NavMenu = (props) => {
  const {cartLength, favLength} = props
  return (
    <nav className="navbar">
      <Link className="navbar__link" to={"/"}>Home</Link>
      <Link className="navbar__link" to={"/cart"}><Cart cartLength={cartLength} /></Link>
      <Link className="navbar__link" to={"/favorites"}><Favorites favLength={favLength} /></Link>
    </nav>
  )
};

export default NavMenu;