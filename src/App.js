import React from 'react';
import { useState, useEffect } from 'react';
import './App.scss'
import Header from "./components/Header/Header";
import NavMenu from "./components/NavMenu/NavMenu";
import AppRoutes from "./routes/AppRoutes";
import Actions from "./components/Modal/Actions/Actions";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import initialStateValues from "./initialState";

const App = () => {

   const [popupName, setPopupName] = useState(initialStateValues.popupName),
      [goods, setGoods] = useState(initialStateValues.goods),
      [favorites, setFavorites] = useState(initialStateValues.favorites),
      [cart, setCart] = useState(initialStateValues.cart),
      [currentItem, setCurrentItem] = useState(initialStateValues.currentItem);

   const getGoods = () => {
      fetch('goods.JSON')
         .then(res => res.json())
         .then(data => {
            setGoods(data);
            // console.log(data);
         })
   }

   useEffect(getGoods);

   const headerProps = {
      logo: 'logo.webp',
      nav: <NavMenu cartLength={cart.length} favLength={favorites.length} />,
   }

   const catalogProps = {
      goods: goods,
      favorites: favorites,
      addToFav: toggleFav,
      showModal: activateCartModal
   }

   const cartProps = {
      cart: cart,
      removeAction: activateRemoveModal,
   }

   const favProps = {
      goods: favorites.map(item => {
         return goods.find(el => el.article === item);
      }),
      favorites: favorites,
      addToFav: toggleFav,
      showModal: activateCartModal
   }

   const cartModalProps = {
      header: `Do you want to add ${currentItem.name} to cart?`,
      text: `You have ${cart.length} items in cart`,
      closeButton: true,
      close: toggleModal,
      modifier: 'cart',
      actions: <Actions buttons={[
         <Button key="0"
            text="Add"
            handler={() => addToCart(currentItem)}
            modifier="cart-modal" />,
         <Button key="1"
            text="Close"
            handler={toggleModal}
            modifier="cart-modal" />]}
      />
   }

   const removeModalProps = {
      full: {
         header: `Do you want to remove ${currentItem.name} from cart?`,
         text: `You have ${cart.filter(el => el.article === currentItem.article).length} items in cart`,
         closeButton: true,
         close: toggleModal,
         modifier: 'remove-file',
         actions: <Actions buttons={[
            <Button key="0"
               text="Yes"
               handler={() => removeFromCart(currentItem.article)}
               modifier="remove-file-modal" />,
            <Button key="1"
               text="No"
               handler={toggleModal}
               modifier="remove-file-modal" />]}
         />
      },
      empty: {
         header: "Items were successfully removed",
         text: `You have ${cart.length} other items in cart`,
         closeButton: true,
         close: toggleModal,
         modifier: 'remove-file',
         actions: <Actions buttons={[

            <Button key="1"
               text="Close"
               handler={toggleModal}
               modifier="remove-file-modal" />]}
         />
      }
   }
   const cartModal = <Modal {...cartModalProps} />,
      removeModal = (props) => <Modal {...props} />

   function toggleFav(article) {
      const fav = favorites;

      if (!fav.includes(article)) {
         fav.push(article)

      } else {
         fav.splice(fav.indexOf(article), 1)
      }

      setFavorites(fav);
      localStorage.setItem('favorites', JSON.stringify(favorites))
   }

   function addToCart(item) {
      const currentCart = cart;
      currentCart.push(item);
      setCart(currentCart);
      localStorage.setItem('cart', JSON.stringify(currentCart));
      toggleModal();
   }

   function removeFromCart(article) {
      const currentCart = [...cart];
      const itemToRemove = currentCart.find(item => item.article === article);

      currentCart.splice(currentCart.indexOf(itemToRemove), 1);
      setCart(currentCart);
      localStorage.setItem('cart', JSON.stringify(currentCart));

      if (currentCart.filter(el => el.article === currentItem.article).length === 0) {
         toggleModal('remove-from-cart-close')
      }
   }

   function toggleModal(name = null) {
      setPopupName(name);
   }

   function activateCartModal(props) {
      setCurrentItem({
         name: props.name,
         article: props.article,
         price: props.price,
         color: props.color,
         image: props.image,
      });

      toggleModal('add-to-cart');
   }

   function activateRemoveModal(props) {
      setCurrentItem({
         name: props.name,
         article: props.article,
         price: props.price,
         color: props.color,
         image: props.image,
      });

      toggleModal('remove-from-cart');
   }

   function renderActiveModal(name) {
      if (name === 'add-to-cart') {
         return cartModal;
      } else if (name === 'remove-from-cart') {
         return removeModal(removeModalProps.full);
      } else if (name === 'remove-from-cart-close') {
         return removeModal(removeModalProps.empty)
      } else {
         return null;
      }
   }

   return (
      <div className="App">
         <Header {...headerProps} />
         <AppRoutes catalogProps={catalogProps} cartProps={cartProps} favProps={favProps} />
         {renderActiveModal(popupName)}
      </div>
   )
}
export default App;
