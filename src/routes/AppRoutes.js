import React from 'react';
import Catalog from "../components/Catalog/Catalog";
import Cart from "../components/Cart/Cart"
import { Routes, Route } from 'react-router-dom';


const AppRoutes = (props) => {
  const { catalogProps, cartProps, favProps } = props;

  return (
    <>
      <Routes>
        <Route exact path='/' element={<Catalog {...catalogProps} />} />
        <Route exact path='/favorites' element={<Catalog {...favProps} />} />
        <Route exact path='/cart' element={<Cart {...cartProps} />} />
      </Routes>
    </>
  );
};

export default AppRoutes;